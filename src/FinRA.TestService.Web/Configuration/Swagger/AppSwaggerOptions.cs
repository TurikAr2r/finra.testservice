﻿using JetBrains.Annotations;

namespace FinRA.TestService.Web.Configuration.Swagger
{
    [PublicAPI]
    public class AppSwaggerOptions
    {
        public bool UseSwagger { get; set; }
    }
}