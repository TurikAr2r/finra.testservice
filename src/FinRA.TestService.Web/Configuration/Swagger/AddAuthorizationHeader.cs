﻿using JetBrains.Annotations;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace FinRA.TestService.Web.Configuration.Swagger
{
    [UsedImplicitly]
    public class AddAuthorizationHeader : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "Authorization",
                In = ParameterLocation.Header,
                Description = "Хидер Basic авторизации",
                Required = false,
                Schema = new OpenApiSchema()
                {
                    Type = "String",
                    Default = new OpenApiString(""),
                    
                }
            });
        }
    }
}