﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OpenTracing;

namespace FinRA.TestService.Web.Configuration.Middlewares
{
    public class TracingMiddleware
    {
        private readonly RequestDelegate _next;

        public TracingMiddleware(RequestDelegate next) => this._next = next;

        private static string ServiceInfo => LoggingSharedData.ServiceName + "." + LoggingSharedData.ServiceVersion;

        public async Task Invoke(HttpContext context, ITracer tracer)
        {
            using (tracer.BuildSpan(ServiceInfo).StartActive(true))
                await this._next(context);
        }
    }

    public class LoggingSharedData
    {
        /// <summary>Имя сервиса, осуществляющего логирование</summary>
        public static string ServiceName;
        /// <summary>Версия сервиса, осуществляющего логирование</summary>
        public static string ServiceVersion;
    }
}