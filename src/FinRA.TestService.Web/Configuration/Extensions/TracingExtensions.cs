﻿using System;
using FinRA.TestService.Web.Configuration.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using OpenTracing;

namespace FinRA.TestService.Web.Configuration.Extensions
{
    public static class TracingExtensions
    {
        /// <summary>
        /// Регистрация трассировки в коллекции сервисов
        /// </summary>
        /// <param name="serviceCollection"></param>
        /// <param name="buildFunc"></param>
        /// <returns></returns>
        public static IServiceCollection AddTracing(
            this IServiceCollection serviceCollection,
            Func<ITracer> buildFunc)
        {
            return serviceCollection.AddScoped<ITracer>((Func<IServiceProvider, ITracer>)(p => buildFunc()));
        }

        /// <summary>
        /// Добавление трассировки в pipeline обработки запроса
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseTracing(this IApplicationBuilder builder) =>
            builder.UseMiddleware<TracingMiddleware>();
    }
}