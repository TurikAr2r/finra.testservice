using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json.Serialization;
using AutoMapper;
using FinRA.TestService;
using FinRA.TestService.Mapping;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using OpenTracing.Mock;
using FinRA.TestService.Controllers;
using FinRA.TestService.Controllers.Attributes;
using FinRA.TestService.Controllers.Configuration;
using FinRA.TestService.Storage.PostgreSql;
using FinRA.TestService.Web.Configuration;
using FinRA.TestService.Web.Configuration.HealthCheck;
using FinRA.TestService.Web.Configuration.Swagger;
using FinRA.TestService.Web.Configuration.Extensions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Server.HttpSys;

namespace FinRA.TestService.Web
{
    public class Startup
    {
        public static string BasePath;
        public static string BuildVersion;
        public static string BuildDate;
        public static string AspNetCoreEnvironment;
        public static string HostName;

        private const string CorsPolicyName = "CorsPolicy";
        private IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            BasePath = string.IsNullOrWhiteSpace(Configuration["GlobalPrefix"])
                ? ""
                : $"/{Configuration["GlobalPrefix"].Trim('/')}";
            BuildVersion = Configuration["BUILD_VERSION"] ?? string.Empty;
            BuildDate = Configuration["BUILD_DATE"] ?? string.Empty;
            AspNetCoreEnvironment = Configuration["ASPNETCORE_ENVIRONMENT"] ?? string.Empty;
            HostName = Configuration["HOSTNAME"] ?? string.Empty;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(new MapperConfiguration(mc =>
                {
                    mc.AddProfile(new ServiceMappingProfile());
                    mc.AddProfile(new ControllersMappingProfile());
                })
                .CreateMapper());

            services.AddHealthChecks()
                // .AddDbContextCheck<AppDbContext>()
                .AddCheck<MemoryHealthCheck>("memory")
                .AddCheck<ThreadPoolHealthCheck>("threadPool");
            
            services.Configure<AppSwaggerOptions>(Configuration);

            services.AddTracing(() => new MockTracer());
            services.AddDomain();

            services.AddPostgreSqlStorage(options =>
            {
                options.UseNpgsql(Configuration["DbConnectionString"], builder => { builder.CommandTimeout(15); });
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            });

            services.AddMvc(opt => { })
                .AddApi()
                .AddControllersAsServices()
                .AddNewtonsoftJson(opt => { opt.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc; })
                .AddJsonOptions(opt => { opt.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()); });
            // configure basic authentication 
            services.AddAuthentication("BasicAuthentication")
                .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", null);

            services.AddAuthorization();
                services.AddValidation();
            services.AddHostedService<HostedService>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Тестовое задание",
                    Version = $"v1 {BuildVersion} {BuildDate} {AspNetCoreEnvironment} {HostName}".Trim(),
                    Description = $"Username = {Configuration["BasicAuthConfiguration:Username"]}; Password = {Configuration["BasicAuthConfiguration:Password"]}"
                });
                c.AddSecurityDefinition("BasicAuth", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    Scheme = "Basic"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "BasicAuth" }
                        },
                        new string[] {}
                    }
                });
                //c.OperationFilter<AddAuthorizationHeader>();
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "FinRA.TestService.Contracts.xml"));
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory,
                    "FinRa.TestService.Controllers.xml"));
            });
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory,
            Microsoft.Extensions.Logging.ILogger<Startup> logger, IOptions<AppSwaggerOptions> swaggerOptions,
            IServiceProvider serviceProvider)
        {
            app.UsePathBase(new PathString(BasePath));

            app.UseHealthChecks("/health", HealthCheckConfiguration.DefaultRules());
            app.UseHealthChecks("/health/full", HealthCheckConfiguration.FullRules());

           // MigrationsRunner.ApplyMigrations(logger, serviceProvider, "FinRA.TestService").Wait();

            if (swaggerOptions.Value.UseSwagger)
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "FinRA.TestService API V1");
                    c.RoutePrefix = "swagger";
                });
            }
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseTracing();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}