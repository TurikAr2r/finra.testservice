﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FinRA.TestService.Storage.PostgreSql;
using FinRA.TestService.Storage.PostgreSql.DataSeed;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FinRA.TestService.Web
{
    public class HostedService : IHostedService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<HostedService> _logger;

        public HostedService(IServiceProvider serviceProvider, ILoggerFactory loggerFactory )
        {
            _logger = loggerFactory.CreateLogger<HostedService>();
            _serviceProvider = serviceProvider;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            MigrationsRunner.ApplyMigrations(_logger, _serviceProvider, "FinRA.TestService").Wait();
            
            using var scope = _serviceProvider.CreateScope();
            var dataSeeder = scope.ServiceProvider.GetRequiredService<DataSeedManager>();

            await dataSeeder.SeedDevelopmentData();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}