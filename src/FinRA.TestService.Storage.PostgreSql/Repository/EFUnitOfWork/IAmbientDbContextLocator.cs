﻿using FinRA.TestService.Storage.PostgreSql;

namespace FinRA.TestService.Storage.PostgreSql.Repository.EFUnitOfWork
{
    /// <summary>
    /// Convenience methods to retrieve ambient DbContext instances. 
    /// </summary>
    public interface IAmbientDbContextLocator
    {
        /// <summary>
        /// If called within the scope of a DbContextScope, gets or creates 
        /// the ambient DbContext instance for the provided DbContext type. 
        /// 
        /// Otherwise returns null. 
        /// </summary>
        //TODO костыль, потому что dbContext не резолвится
        AppDbContext GetAppDbContext();
    }
}