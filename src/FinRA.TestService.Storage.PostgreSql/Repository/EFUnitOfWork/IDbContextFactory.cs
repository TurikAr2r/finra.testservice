﻿using Microsoft.EntityFrameworkCore;

namespace FinRA.TestService.Storage.PostgreSql.Repository.EFUnitOfWork
{
    public interface IDbContextFactory {
        TDbContext CreateDbContext<TDbContext> () where TDbContext : DbContext;
    }
}