﻿using System.Data;
using FinRA.TestService.Abstractions.EFUnitOfWork;
using FinRA.TestService.Storage.PostgreSql;

namespace FinRA.TestService.Storage.PostgreSql.Repository.EFUnitOfWork
{
    public class EfUnitOfWorkFactory : IUnitOfWorkFactory {
        private readonly AppDbContext _dbContext;

        public EfUnitOfWorkFactory (AppDbContext dbContext) {
            _dbContext = dbContext;
        }

        public IUnitOfWork Create (IsolationLevel isolationLevel) {
            return new EfUnitOfWork(isolationLevel);
        }

        public IUnitOfWork Create () {
            return Create(IsolationLevel.ReadCommitted);
        }
    }
}