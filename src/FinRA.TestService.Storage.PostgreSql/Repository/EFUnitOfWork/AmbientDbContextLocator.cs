﻿using System;
using FinRA.TestService.Storage.PostgreSql;

namespace FinRA.TestService.Storage.PostgreSql.Repository.EFUnitOfWork
{
    public class AmbientDbContextLocator : IAmbientDbContextLocator
    {
        private readonly AppDbContext _defaultDbContext;
        private readonly IServiceProvider _serviceProvider;
        public AmbientDbContextLocator(AppDbContext defaultDbContext, IServiceProvider serviceProvider)
        {
            _defaultDbContext = defaultDbContext;
            _serviceProvider = serviceProvider;
        }

        public AppDbContext GetAppDbContext()
        {
            var ambientDbContextScope = EfUnitOfWork.GetAmbientScope();
            return ambientDbContextScope == null ? _defaultDbContext : ambientDbContextScope.DbContexts.Get(_serviceProvider);
        }
    }
}