﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using FinRA.TestService.Abstractions;
using FinRA.TestService.Storage.PostgreSql.Repository.EFUnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace FinRA.TestService.Storage.PostgreSql.Repository
{
    /// <summary>
    /// Реализация репозитория с поддержкой управления транзакциями
    /// </summary>
    public class Repository : IRepository
    {
        private readonly IAmbientDbContextLocator _ambientDbContextLocator;
        //private Dictionary<DbContext, IDbContextTransaction> _transactions;

        public Repository(IAmbientDbContextLocator contextLocator) => _ambientDbContextLocator = contextLocator;

        protected DbContext CurrentContext
        {
            get
            {
                var context = _ambientDbContextLocator.GetAppDbContext();
                if (context == null)
                    throw new InvalidOperationException("No TelemarketingContext");

                return context;
            }
        }

        public void SaveChanges() => CurrentContext.SaveChanges();

        public async Task SaveChangesAsync() => await CurrentContext.SaveChangesAsync();

        public IQueryable<TEntity> Select<TEntity>() where TEntity : class, IEntity
        {
            var query = CurrentContext.Set<TEntity>();
            //_transactions.FirstOrDefault(x=>x.Key == _context).Value.Commit();
            return query;
        }

        public IQueryable<TEntity> Select<TEntity>(Expression<Func<TEntity, bool>> expr,
            params Expression<Func<TEntity, object>>[] includePath) where TEntity : class, IEntity
        {
            {
                var query = CurrentContext.Set<TEntity>().Where(expr);

                foreach (var include in includePath)
                {
                    query = query.Include(include);
                }

                //_transactions.FirstOrDefault(x=>Equals(x.Key, _context)).Value.Commit();
                return query;
            }
        }

        public TEntity Single<TEntity>(object key) where TEntity : class, IEntity
        {
            var result = CurrentContext.Set<TEntity>().Find(key);
            //_transactions.FirstOrDefault(x=>Equals(x.Key, _context)).Value.Commit();
            return result;
        }

        public TEntity Single<TEntity>(Expression<Func<TEntity, bool>> expr) where TEntity : class, IEntity
        {
            var result = CurrentContext.Set<TEntity>().Where(expr).FirstOrDefault();
            //_transactions.FirstOrDefault(x=>Equals(x.Key, _context)).Value.Commit();
            return result;
        }

        public async Task<TEntity> SingleAsync<TEntity>(Expression<Func<TEntity, bool>> expr)
            where TEntity : class, IEntity
        {
            var result = await CurrentContext.Set<TEntity>().Where(expr).FirstOrDefaultAsync();
            //_transactions.FirstOrDefault(x=>Equals(x.Key, _context)).Value.Commit();
            return result;
        }

        public async Task<TEntity> SingleOrThrowAsync<TEntity>(Expression<Func<TEntity, bool>> expr)
            where TEntity : class, IEntity
        {
            var result = await CurrentContext.Set<TEntity>().SingleOrDefaultAsync(expr);
            //_transactions.FirstOrDefault(x=>Equals(x.Key, _context)).Value.Commit();
            return result;
        }

        public async Task<TEntity> SingleAsync<TEntity>(object key) where TEntity : class, IEntity
        {
            var result = await CurrentContext.Set<TEntity>().FindAsync(key);
            //_transactions.FirstOrDefault(x=>Equals(x.Key, _context)).Value.Commit();
            return result;
        }

        public void Add<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            CurrentContext.Set<TEntity>().Add(entity);
        }

        public void Remove<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            CurrentContext.Set<TEntity>().Remove(entity);
            CurrentContext.SaveChanges();
        }

        public void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity
        {
            CurrentContext.Set<TEntity>().RemoveRange(entities);
            CurrentContext.SaveChanges();
        }

        public async Task RemoveAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            CurrentContext.Set<TEntity>().Remove(entity);
            await CurrentContext.SaveChangesAsync();
        }

        public TEntity Save<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            CurrentContext.Entry(entity).State = CurrentContext.Entry(entity).State == EntityState.Detached
                ? EntityState.Added
                : EntityState.Modified;

            CurrentContext.SaveChanges();

            return entity;
        }

        public async Task<TEntity> SaveAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            CurrentContext.Entry(entity).State = CurrentContext.Entry(entity).State == EntityState.Detached
                ? EntityState.Added
                : EntityState.Modified;


            await CurrentContext.SaveChangesAsync();

            return entity;
        }

        public virtual long Count<TEntity>(Expression<Func<TEntity, bool>> selector)
            where TEntity : class, IEntity
        {
            var c = Select<TEntity>();
            var q = c.Count(selector);
            return q;
        }
    }

    /// <summary>
    /// Реализация репозитория без поддержки управления транзакциями
    /// </summary>
    // public class Repository : IRepository
    // {
    //     //private readonly ILogger _logger;
    //     //protected readonly DbSet<TEntity> _dbSet;
    //
    //     private readonly AppDbContext _appDbContext;
    //
    //     public Repository(AppDbContext appDbContext)
    //     {
    //        // _logger = logManager.GetLogger<Repository>();
    //         _appDbContext = appDbContext;
    //     }
    //     protected DbContext CurrentContext
    //     {
    //         get
    //         {
    //             if (_appDbContext == null)
    //                 throw new InvalidOperationException("No TelemarketingContext");
    //
    //             return _appDbContext;
    //         }
    //     }
    //     public void SaveChanges()
    //     {
    //         CurrentContext.SaveChanges();
    //     }
    //
    //     public async Task SaveChangesAsync()
    //     {
    //         await CurrentContext.SaveChangesAsync();
    //     }
    //
    //     public IQueryable<TEntity> Select<TEntity>() where TEntity : class, IEntity
    //     {
    //         return CurrentContext.Set<TEntity>();
    //     }
    //
    //     public IQueryable<TEntity> Select<TEntity>(Expression<Func<TEntity, bool>> expr, params Expression<Func<TEntity, object>>[] includePath) where TEntity : class, IEntity
    //     {
    //         var query = CurrentContext.Set<TEntity>().Where(expr);
    //
    //         foreach (var include in includePath)
    //         {
    //             query.Include(include);
    //         }
    //
    //         return query;
    //     }
    //
    //     public TEntity Single<TEntity>(object key) where TEntity : class, IEntity
    //     {
    //         return CurrentContext.Set<TEntity>().Find(key);
    //     }
    //
    //     public TEntity Single<TEntity>(Expression<Func<TEntity, bool>> expr) where TEntity : class, IEntity
    //     {
    //         return CurrentContext.Set<TEntity>().Where(expr).FirstOrDefault();
    //     }
    //     public async Task<TEntity> SingleAsync<TEntity>(Expression<Func<TEntity, bool>> expr) where TEntity : class, IEntity
    //     {
    //         return await CurrentContext.Set<TEntity>().Where(expr).FirstOrDefaultAsync();
    //     }
    //
    //     public async Task<TEntity> SingleAsync<TEntity>(object key) where TEntity : class, IEntity
    //     {
    //         return await CurrentContext.Set<TEntity>().FindAsync(key);
    //     }
    //
    //     public void Add<TEntity>(TEntity entity) where TEntity : class, IEntity
    //     {
    //         CurrentContext.Set<TEntity>().Add(entity);
    //     }
    //
    //     public void Remove<TEntity>(TEntity entity) where TEntity : class, IEntity
    //     {
    //         CurrentContext.Set<TEntity>().Remove(entity);
    //         CurrentContext.SaveChanges();
    //     }
    //
    //     public void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity
    //     {
    //         CurrentContext.Set<TEntity>().RemoveRange(entities);
    //         CurrentContext.SaveChanges();
    //     }
    //
    //     public async Task RemoveAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
    //     {
    //         CurrentContext.Set<TEntity>().Remove(entity);
    //         await CurrentContext.SaveChangesAsync();
    //     }
    //
    //     public TEntity Save<TEntity>(TEntity entity) where TEntity : class, IEntity
    //     {
    //         if (entity == null)
    //         {
    //             throw new ArgumentNullException("entity");
    //         }
    //         CurrentContext.Entry(entity).State = CurrentContext.Entry(entity).State == EntityState.Detached ? EntityState.Added : EntityState.Modified;
    //
    //         CurrentContext.SaveChanges();
    //
    //         return entity;
    //     }
    //     public async Task<TEntity> SaveAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
    //     {
    //         if (entity == null)
    //         {
    //             throw new ArgumentNullException("entity");
    //         }
    //         CurrentContext.Entry(entity).State = CurrentContext.Entry(entity).State == EntityState.Detached ? EntityState.Added : EntityState.Modified;
    //
    //
    //         await CurrentContext.SaveChangesAsync();
    //
    //         return entity;
    //     }
    // }
}