﻿using System;
using System.Threading.Tasks;
using FinRA.TestService.Abstractions;
using FinRA.TestService.Abstractions.EFUnitOfWork;
using FinRA.TestService.Abstractions.Storage;
using FinRA.TestService.Helpers.Extensions;
using FinRA.TestService.Storage.PostgreSql.DataSeed;
using FinRA.TestService.Storage.PostgreSql.Repository.EFUnitOfWork;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FinRA.TestService.Storage.PostgreSql
{
    public static class Entry
    {
        private static readonly Action<DbContextOptionsBuilder> DefaultOptionsAction = (options) =>
        { };

        public static IServiceCollection AddPostgreSqlStorage(this IServiceCollection serviceCollection, Action<DbContextOptionsBuilder> optionsAction)
        {
            serviceCollection.AddDbContext<AppDbContext>(optionsAction ?? DefaultOptionsAction);
            
            serviceCollection.AddScoped<IAmbientDbContextLocator, AmbientDbContextLocator>();
            serviceCollection.AddTransient<IRepository, Repository.Repository>();
            serviceCollection.AddTransient<IUnitOfWorkFactory, EfUnitOfWorkFactory>();
            serviceCollection.RegisterGenericAbstractImplementations(typeof(IDataSeeder));
            serviceCollection.AddTransient<DataSeedManager>();

            return serviceCollection;
        }
    }
}