// ef commands templates for the Rider's terminal

cd src/SomeProject.Warehouse.Storage.PostgreSql
dotnet restore
dotnet ef -h
dotnet ef migrations add Initial --verbose --project ../../src/FinRA.TestService.Storage.PostgreSql --startup-project ../../src/FinRA.TestService.Web
dotnet ef database update --verbose --project ../../src/FinRA.TestService.Storage.PostgreSql --startup-project ../../src/FinRA.TestService.Web