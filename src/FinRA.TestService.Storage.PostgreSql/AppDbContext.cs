﻿using FinRA.TestService.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace FinRA.TestService.Storage.PostgreSql
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        { }
       
        public DbSet<AssetEntity> Assets { get; set; }
        public DbSet<IssuerEntity> Issuers { get; set; }
        public DbSet<StockExchangeEntity> StockExchanges { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<StockExchangeEntity>().HasIndex(x => x.Code).IsUnique();
            builder.Entity<AssetEntity>().HasAlternateKey(x => x.Isin);
            base.OnModelCreating(builder);
            
        }
    }
}