﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FinRA.TestService.Abstractions;
using FinRA.TestService.Abstractions.Storage;

namespace FinRA.TestService.Storage.PostgreSql.DataSeed
{
    public abstract class DataSeeder<TEntity> : IDataSeeder where TEntity : class, IEntity
    {
        protected readonly AppDbContext DbContext;

        protected abstract IEnumerable<TEntity> DevelopmentSeedData { get; set; }

        protected DataSeeder(AppDbContext dbContext) => DbContext = dbContext;

        public async Task DevelopmentDataSeed() => await SeedData();

        protected abstract Task SeedData();
    }
}