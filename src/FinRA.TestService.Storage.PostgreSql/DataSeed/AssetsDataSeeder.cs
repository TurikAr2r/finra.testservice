﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinRA.TestService.Contracts.Enums;
using FinRA.TestService.Enums;
using FinRA.TestService.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace FinRA.TestService.Storage.PostgreSql.DataSeed
{
    public class AssetsDataSeeder : DataSeeder<AssetEntity>
    {
        public AssetsDataSeeder(AppDbContext dbContext) : base(dbContext)
        {
        }

        protected override IEnumerable<AssetEntity> DevelopmentSeedData { get; set; } = new List<AssetEntity>
        {
            new AssetEntity
            {
                Id = new Guid("28c28589-26b6-4da4-ae9d-64bb55897138"), Ticker = "SBER", Isin = "RU0009029540",
                Name = "Сбербанк России, акция обыкновенная", BaseCurrency = "RUB",
                AssetCirculationPeriodStart = new DateTimeOffset(new DateTime(2007, 7, 20)),
                StockType = StockType.Common, IssuerId = new Guid("61b6b0f3-1786-4cd8-91f2-6ec24df919e4"),
                StockExchangeId = 1, LotSize = 10, Type = AssetType.Stock
            },
            new AssetEntity
            {
                Id = new Guid("c26f6095-34ab-4764-9724-ceadbd84a94d"), Ticker = "SBERP", Isin = "RU0009029557",
                Name = "Сбербанк России, акция привилегированная", BaseCurrency = "RUB",
                AssetCirculationPeriodStart = new DateTimeOffset(new DateTime(2007, 7, 16)),
                StockType = StockType.Privileged, IssuerId = new Guid("61b6b0f3-1786-4cd8-91f2-6ec24df919e4"),
                StockExchangeId = 1, LotSize = 10, Type = AssetType.Stock
            },
            new AssetEntity { Id = new Guid("999e49b3-14b2-4fa0-8a6d-8e5c078fe6f0"), Ticker = "MTSS", Isin = "RU0007775219",
                Name = "МТС, акция обыкновенная", BaseCurrency = "RUB",
                AssetCirculationPeriodStart = new DateTimeOffset(new DateTime(2004, 2, 11)),
                StockType = StockType.Common, IssuerId = new Guid("33c36ab8-ff2a-44e5-91db-4d2e3728eed1"),
                StockExchangeId = 2, LotSize = 100, Type = AssetType.Stock},
            new AssetEntity { Id = new Guid("43881617-2070-4763-a0a8-4bb315495abe"),Ticker = "SNGS", Isin = "RU0008926258",
                Name = "Сургутнефтегаз, акция обыкновенная", BaseCurrency = "RUB",
                AssetCirculationPeriodStart = new DateTimeOffset(new DateTime(2005, 1, 11)),
                StockType = StockType.Common, IssuerId = new Guid("06b507e2-0090-40e0-8697-94fae286cf8c"),
                StockExchangeId = 1, LotSize = 10, Type = AssetType.Stock },
            new AssetEntity { Id = new Guid("c0c16fb3-07a0-44c7-bcf0-2af008940235"),Ticker = "ALRS", Isin = "RU0007252813",
                Name = "Алроса, акция обыкновенная", BaseCurrency = "RUB",
                AssetCirculationPeriodStart = new DateTimeOffset(new DateTime(2011, 11, 29)),
                StockType = StockType.Common, IssuerId = new Guid("e55c471e-4cb2-438f-8d77-cc52e161fae4"),
                StockExchangeId = 1, LotSize = 10, Type = AssetType.Stock  },
            new AssetEntity { Id = new Guid("6242ca5b-514f-4b26-a7b2-86d5f527e5a2"), Ticker = "AAPL-RM", Isin = "US0378331005",
                Name = "Apple, акция обыкновенная", BaseCurrency = "USD",
                AssetCirculationPeriodStart = new DateTimeOffset(new DateTime(2020, 9, 8)),
                StockType = StockType.Common, IssuerId = new Guid("d11f50a4-f26f-47b0-951e-145b9b18652b"),
                StockExchangeId = 1, LotSize = 10, Type = AssetType.Stock },
            new AssetEntity { Id = new Guid("f645ff8b-4a5d-4fc0-b268-4d03fcd7244f"), Ticker = "YNDX", Isin = "NL0009805522",
                Name = "ЯНДЕКС Н.В., акция обыкновенная", BaseCurrency = "EUR",
                AssetCirculationPeriodStart = new DateTimeOffset(new DateTime(2014, 6, 4)),
                StockType = StockType.Common, IssuerId = new Guid("29c2a3f0-5897-422a-ab13-a7aa88e842b0"),
                StockExchangeId = 2, LotSize = 1, Type = AssetType.Stock},
            new AssetEntity { Id = new Guid("e93fe562-290d-4c4e-8a04-749c043857c6"), Ticker = "RU000A101C89", Isin = "RU000A101C89",
                Name = "Sber 001P-SBER15", BaseCurrency = "RUB",
                AssetCirculationPeriodStart = new DateTimeOffset(new DateTime(2020, 1, 20)),
                BondType = BondType.State, IssuerId = new Guid("61b6b0f3-1786-4cd8-91f2-6ec24df919e4"),
                StockExchangeId = 1, LotSize = 10 , Type = AssetType.Bond},
            new AssetEntity { Id = new Guid("08a8c1ce-9022-484f-a790-648f2840fffa"),Ticker = "RU000A1013J4", Isin = "RU000A1013J4",
                Name = "SberIOS 001P-177R GMKN 100", BaseCurrency = "RUB",
                AssetCirculationPeriodStart = new DateTimeOffset(new DateTime(2019, 6, 19)),
                BondType = BondType.Corporate, IssuerId = new Guid("61b6b0f3-1786-4cd8-91f2-6ec24df919e4"),
                StockExchangeId = 1, LotSize = 10 , Type = AssetType.Bond},
        };

        protected override async Task SeedData()
        {
            var entities = await DbContext.Assets.AsQueryable()
                .Where(mapping =>
                    DevelopmentSeedData.Select(identityMapping => identityMapping.Id).Contains(mapping.Id))
                .ToArrayAsync();

            DbContext.Assets.RemoveRange(entities);
            await DbContext.SaveChangesAsync();

            DbContext.Assets.AddRange(DevelopmentSeedData);
            await DbContext.SaveChangesAsync();
        }
    }
}