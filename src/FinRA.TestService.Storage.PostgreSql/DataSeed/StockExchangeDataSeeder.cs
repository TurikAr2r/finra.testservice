﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinRA.TestService.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace FinRA.TestService.Storage.PostgreSql.DataSeed
{
    public class StockExchangeDataSeeder : DataSeeder<StockExchangeEntity>
    {
        public StockExchangeDataSeeder(AppDbContext dbContext) : base(dbContext)
        {
        }

        protected override IEnumerable<StockExchangeEntity> DevelopmentSeedData { get; set; } =
            new List<StockExchangeEntity>
            {
                new StockExchangeEntity
                {
                    Id = 1, Name = "Московская биржа", Code = "MISX"

                },
                new StockExchangeEntity
                {
                    Id = 2, Name = "Санкт-Петербуржская биржа", Code = "SPB"
                }
            };
        protected override async Task SeedData()
        {
            var entities = await DbContext.StockExchanges.AsQueryable()
                .Where(exchangeEntity => DevelopmentSeedData.Select(stockExchangeEntity => stockExchangeEntity.Id).Contains(exchangeEntity.Id))
                .ToArrayAsync();
            
            DbContext.StockExchanges.RemoveRange(entities);
            DbContext.SaveChanges();

            DbContext.StockExchanges.AddRange(DevelopmentSeedData);
            await DbContext.SaveChangesAsync();
        }
    }
}