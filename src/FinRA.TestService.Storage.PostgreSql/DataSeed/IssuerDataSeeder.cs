﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinRA.TestService.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace FinRA.TestService.Storage.PostgreSql.DataSeed
{
    public class IssuerDataSeeder : DataSeeder<IssuerEntity>
    {
        public IssuerDataSeeder(AppDbContext dbContext) : base(dbContext)
        {
        }

        protected override IEnumerable<IssuerEntity> DevelopmentSeedData { get; set; } = new List<IssuerEntity>
        {
            new IssuerEntity
            {
                Id = new Guid("61b6b0f3-1786-4cd8-91f2-6ec24df919e4"),
                Name = "Сбербанк России",
                Country = "Россия",
                Industry = "Финансы"
            },
            new IssuerEntity
            {
                Id = new Guid("33c36ab8-ff2a-44e5-91db-4d2e3728eed1"),
                Name = "МТС",
                Country = "Россия",
                Industry = "Телекоммуникации"
            },
            new IssuerEntity
            {
                Id = new Guid("06b507e2-0090-40e0-8697-94fae286cf8c"),
                Name = "Супернефтегаз",
                Country = "Россия",
                Industry = "Нефть и газ"
            },
            new IssuerEntity
            {
                Id = new Guid("e55c471e-4cb2-438f-8d77-cc52e161fae4"),
                Name = "Алроса",
                Country = "Россия",
                Industry = "Добыча ископаемых"
            },
            new IssuerEntity
            {
                Id = new Guid("d11f50a4-f26f-47b0-951e-145b9b18652b"),
                Name = "Apple",
                Country = "США",
                Industry = "Технологии"
            },
            new IssuerEntity
            {
                Id = new Guid("29c2a3f0-5897-422a-ab13-a7aa88e842b0"),
                Name = "Яндекс",
                Country = "Россия",
                Industry = "IT"
            },
            new IssuerEntity
            {
                Id = new Guid("20760e19-2b74-4e18-a951-7edd5465a66a"),
                Name = "Mail",
                Country = "Россия",
                Industry = "IT"
            }
        };
        protected override async Task SeedData()
        {
            var entities = await DbContext.Issuers.AsQueryable()
                .Where(issuerEntity => DevelopmentSeedData.Select(entity => entity.Id).Contains(issuerEntity.Id))
                .ToArrayAsync();
            
            DbContext.Issuers.RemoveRange(entities);
            DbContext.SaveChanges();

            DbContext.Issuers.AddRange(DevelopmentSeedData);
            await DbContext.SaveChangesAsync();
        }
    }
}