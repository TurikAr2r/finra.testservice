﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinRA.TestService.Abstractions.Storage;

namespace FinRA.TestService.Storage.PostgreSql.DataSeed
{
    public class DataSeedManager
    {
        private readonly IEnumerable<IDataSeeder> _dataSeeders;

        public DataSeedManager(IEnumerable<IDataSeeder> dataSeeders) => _dataSeeders = dataSeeders;

        public async Task SeedDevelopmentData()
        {
            var stockExchangeDataSeeder =
                _dataSeeders.FirstOrDefault(x => x.GetType() == typeof(StockExchangeDataSeeder));
            await stockExchangeDataSeeder.DevelopmentDataSeed();
            
            var issuerDataSeeder = _dataSeeders.FirstOrDefault(x => x.GetType() == typeof(IssuerDataSeeder));
            await issuerDataSeeder.DevelopmentDataSeed();
            
            var assetsDataSeeder = _dataSeeders.FirstOrDefault(x => x.GetType() == typeof(AssetsDataSeeder));
            await assetsDataSeeder.DevelopmentDataSeed();
        }
    }
}