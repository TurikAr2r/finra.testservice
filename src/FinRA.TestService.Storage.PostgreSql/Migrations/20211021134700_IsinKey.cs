﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FinRA.TestService.Storage.PostgreSql.Migrations
{
    public partial class IsinKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Isin",
                table: "Assets",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_Assets_Isin",
                table: "Assets",
                column: "Isin");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_Assets_Isin",
                table: "Assets");

            migrationBuilder.AlterColumn<string>(
                name: "Isin",
                table: "Assets",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");
        }
    }
}
