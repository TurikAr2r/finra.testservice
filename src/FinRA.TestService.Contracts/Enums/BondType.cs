﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace FinRA.TestService.Contracts.Enums
{
    public enum BondType
    {
        [Description("Государственная")]
        [EnumMember(Value = "State")]
        State,
        [Description("Муниципальная")]
        [EnumMember(Value = "Municipal")]
        Municipal,
        [Description("Корпоративная")]
        [EnumMember(Value = "Corporate")]
        Corporate,
    }
}