﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace FinRA.TestService.Contracts.Enums
{
    public enum StockType
    {
        [Description("Обычная")]
        [EnumMember(Value = "Common")]
        Common,
        [Description("Привилегированная")]
        [EnumMember(Value = "Privileged")]
        Privileged
    }
}