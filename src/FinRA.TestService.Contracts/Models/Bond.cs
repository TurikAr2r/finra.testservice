﻿using FinRA.TestService.Contracts.Enums;

namespace FinRA.TestService.Contracts.Models
{
    public class Bond
    {
        public BondType Type { get; set; }
        public string Description { get; set; }
    }
}