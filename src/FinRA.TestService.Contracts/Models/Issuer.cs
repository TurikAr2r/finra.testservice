﻿using System;

namespace FinRA.TestService.Contracts.Models
{
    public class Issuer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }   
        public string Country { get; set; }   
        public string Industry { get; set; }   
    }
}