﻿using FinRA.TestService.Contracts.Enums;

namespace FinRA.TestService.Contracts.Models
{
    public class Stock
    {
        public StockType Type { get; set; }
        public string Description { get; set; }
    }
}