﻿using System;

namespace FinRA.TestService.Contracts.Models
{
    public class Asset
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Isin { get; set; }
        public string Ticker { get; set; }
    }
}