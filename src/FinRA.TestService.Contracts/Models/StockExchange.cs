﻿namespace FinRA.TestService.Contracts.Models
{
    public class StockExchange
    {
        public string Name { get; set; }
        /// <summary>
        /// Код биржы
        /// </summary>
        public string Code { get; set; }
    }
}