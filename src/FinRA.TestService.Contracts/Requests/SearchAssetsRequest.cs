﻿using JetBrains.Annotations;

namespace FinRA.TestService.Contracts.Requests
{
    [PublicAPI]
    public class SearchAssetsRequest
    {
        public string Search { get; set; }
    }
}