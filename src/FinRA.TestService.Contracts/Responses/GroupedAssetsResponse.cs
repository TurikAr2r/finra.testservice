﻿using System.Collections.Generic;
using FinRA.TestService.Contracts.Responses.Assets;

namespace FinRA.TestService.Contracts.Models
{
    public class GroupedAssetsResponse
    {
        public string AssetType { get; set; }
        public List<AssetBaseInfoResponse> Assets { get; set; }
    }
}