﻿using System;
using FinRA.TestService.Contracts.Models;

namespace FinRA.TestService.Contracts.Responses.Assets
{
    public class AssetBaseInfoResponse : Asset
    {
        public string AssetType { get; set; }
        /// <summary>
        /// Код биржи
        /// </summary>
        public string StockExchangeCode { get; set; }
        public string IssuerName { get; set; }
    }
}