﻿using System;
using FinRA.TestService.Contracts.Models;

namespace FinRA.TestService.Contracts.Responses.Assets
{
    public class AssetFullInfoResponse : AssetBaseInfoResponse
    {
        public Models.Issuer Issuer { get; set; }
        public StockExchange StockExchange { get; set; }
        public DateTimeOffset AssetCirculationPeriodStart { get; set; }
        public DateTimeOffset? AssetCirculationPeriodEnd { get; set; }
        public string BaseCurrency { get; set; }
        public long LotSize { get; set; }
        public Stock Stock { get; set; }
        public Bond Bond { get; set; }
    }
}