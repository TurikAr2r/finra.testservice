﻿using System.Collections.Generic;
using FinRA.TestService.Contracts.Models;
using FinRA.TestService.Contracts.Responses.Assets;

namespace FinRA.TestService.Contracts.Responses
{
    public class SearchAssetsResponse
    {
        public List<GroupedAssetsResponse> AssetsGrouped { get; set; }
    }
}