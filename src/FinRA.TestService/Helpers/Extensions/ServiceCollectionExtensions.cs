﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace FinRA.TestService.Helpers.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterGenericAbstractImplementations(
            this IServiceCollection serviceCollection,
            Type type)
        {
            AppDomain.CurrentDomain.GetAssemblies().SelectMany(s
                    => s.GetTypes()).Where(item => item.GetInterfaces()
                    .Any(i => i == type) && !item.IsAbstract && !item.IsInterface).ToList()
                .ForEach(assignedTypes =>
                    serviceCollection.AddTransient(assignedTypes.GetInterfaces().First(), assignedTypes));
            
            return serviceCollection;
        }
    }
}