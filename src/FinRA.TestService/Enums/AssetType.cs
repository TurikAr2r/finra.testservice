﻿using System.ComponentModel;

namespace FinRA.TestService.Enums
{
    public enum AssetType
    {
        [Description("Акция")]
        Stock,
        [Description("Облигация")]
        Bond
    }
}