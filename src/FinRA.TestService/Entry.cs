﻿using FinRA.TestService.Abstractions.Services;
using FinRA.TestService.Services;
using Microsoft.Extensions.DependencyInjection;

namespace FinRA.TestService
{
    public static class Entry
    {
        public static IServiceCollection AddDomain(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IAssetService, AssetService>();
            serviceCollection.AddTransient<IIssuerService, IssuerService>();
            return serviceCollection;
        }
    }
}