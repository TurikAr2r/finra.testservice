﻿using System;
using System.Collections.Generic;
using FinRA.TestService.Abstractions;
using FinRA.TestService.Contracts.Models;

namespace FinRA.TestService.Models.Entities
{
    public class IssuerEntity : Issuer, IEntity
    {
        public Guid Id { get; set; }
        public List<AssetEntity> Assets { get; set; }
    }
}