﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FinRA.TestService.Abstractions;

namespace FinRA.TestService.Models.Entities
{
    public class StockExchangeEntity : Contracts.Models.StockExchange, IEntity
    {
        [Key]
        public long Id { get; set; }
        public List<AssetEntity> Assets { get; set; }
    }
}