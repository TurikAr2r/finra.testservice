﻿using System;
using FinRA.TestService.Abstractions;
using FinRA.TestService.Contracts.Enums;
using FinRA.TestService.Enums;

namespace FinRA.TestService.Models.Entities
{
    public class AssetEntity : Contracts.Models.Asset, IEntity
    {
        public AssetType Type { get; set; }
        public BondType? BondType { get; set; }
        public StockType? StockType { get; set; }
        public string BaseCurrency { get; set; }
        public DateTimeOffset AssetCirculationPeriodStart { get; set; }
        public DateTimeOffset? AssetCirculationPeriodEnd { get; set; }
        public long LotSize { get; set; }
        public Guid IssuerId { get; set; }
        public IssuerEntity Issuer { get; set; }
        
        public long StockExchangeId { get; set; }
        public StockExchangeEntity StockExchange { get; set; }
    }
}