﻿using System;

namespace FinRA.TestService.Abstractions.EFUnitOfWork
{
    public interface IUnitOfWork : IDisposable {
        ///<summary>
        /// Сохраняет все изменения в блоке
        ///</summary>
        void Commit ();
    }
}