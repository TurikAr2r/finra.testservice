﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FinRA.TestService.Abstractions
{
    public interface IRepository
    {
        void SaveChanges();

        Task SaveChangesAsync();

        IQueryable<TEntity> Select<TEntity>() where TEntity : class, IEntity;

        IQueryable<TEntity> Select<TEntity>(Expression<Func<TEntity, bool>> expr,
            params Expression<Func<TEntity, object>>[] includePath) where TEntity : class, IEntity;

        TEntity Single<TEntity>(object key) where TEntity : class, IEntity;

        Task<TEntity> SingleAsync<TEntity>(Expression<Func<TEntity, bool>> expr) where TEntity : class, IEntity;

        Task<TEntity> SingleOrThrowAsync<TEntity>(Expression<Func<TEntity, bool>> expr) where TEntity : class, IEntity;
        Task<TEntity> SingleAsync<TEntity>(object key) where TEntity : class, IEntity;

        TEntity Single<TEntity>(Expression<Func<TEntity, bool>> expr) where TEntity : class, IEntity;

        void Add<TEntity>(TEntity entity) where TEntity : class, IEntity;

        void Remove<TEntity>(TEntity entity) where TEntity : class, IEntity;

        void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity;

        Task RemoveAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        TEntity Save<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task<TEntity> SaveAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

    }
}