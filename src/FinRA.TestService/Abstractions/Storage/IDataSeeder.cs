﻿using System.Threading.Tasks;

namespace FinRA.TestService.Abstractions.Storage
{
    public interface IDataSeeder
    {
        Task DevelopmentDataSeed();
    }
}