﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FinRA.TestService.Abstractions
{
    public abstract class GuidEntity : IEntity
    {
        [Key]
        public Guid Id { get; private set; } = Guid.NewGuid();
    }
}