﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinRA.TestService.Enums;
using FinRA.TestService.Models;
using FinRA.TestService.Models.Entities;

namespace FinRA.TestService.Abstractions.Services
{
    public interface IAssetService
    {
        Task<List<IGrouping<AssetType, AssetEntity>>> SearchAssets(string search, AssetType? assetType = null);
        Task<AssetEntity> GetAssetInfo(Guid issuerId);
        Task<AssetEntity> GetAssetInfoByIsin(string isin);
    }
}