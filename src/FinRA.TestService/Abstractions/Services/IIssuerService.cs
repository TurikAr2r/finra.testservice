﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinRA.TestService.Contracts.Models;
using FinRA.TestService.Models.Entities;

namespace FinRA.TestService.Abstractions.Services
{
    public interface IIssuerService
    {
        Task<List<IssuerEntity>> GetIssuers();
        Task<List<AssetEntity>> GetIssuersAssets(Guid id);
    }
}