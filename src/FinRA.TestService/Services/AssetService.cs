﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinRA.TestService.Abstractions;
using FinRA.TestService.Abstractions.Services;
using FinRA.TestService.Enums;
using FinRA.TestService.Models;
using FinRA.TestService.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace FinRA.TestService.Services
{
    public class AssetService : IAssetService
    {
        private readonly IRepository _repository;

        public AssetService(IRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<IGrouping<AssetType,AssetEntity>>> SearchAssets(string search, AssetType? assetType = null)
        {
            var searchParametr = search.TrimEnd(' ').ToLower();
            var assets = _repository.Select<AssetEntity>(x
                => x.Name.ToLower().Contains(searchParametr)
                   || x.Ticker.ToLower().Contains(searchParametr)
                   || x.Isin.ToLower().Contains(searchParametr)).Include(x=>x.StockExchange).Include(x=>x.Issuer).AsQueryable();
            if (assetType != null)
                assets = assets.Where(x => x.Type == assetType.Value);

            var assetsResult = await assets.ToListAsync();
            var groupedAssets = assetsResult.GroupBy(x=>x.Type);
            return groupedAssets.ToList();
            // return groupedAssets
            //     .Select(groupedAsset 
            //         => new GroupedAssetsResponse { Type = groupedAsset.Key, AssetEntities = groupedAsset.Select(x => x).ToList() })
            //     .ToList();
        }

        public async Task<AssetEntity> GetAssetInfo(Guid assetId)
        {
            var result = await _repository.Select<AssetEntity>(x => x.Id == assetId).Include(x => x.Issuer)
                .Include(x=>x.StockExchange)
                .AsNoTracking().FirstOrDefaultAsync();
            return result;
        }

        public async Task<AssetEntity> GetAssetInfoByIsin(string isin)
        {
            var result = await _repository.Select<AssetEntity>(x => x.Isin.ToLower() == isin.ToLower()).Include(x => x.Issuer)
                .Include(x=>x.StockExchange)
                .AsNoTracking().FirstOrDefaultAsync();
            return result;
        }
    }
}