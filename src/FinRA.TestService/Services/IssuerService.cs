﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinRA.TestService.Abstractions;
using FinRA.TestService.Abstractions.Services;
using FinRA.TestService.Contracts.Models;
using FinRA.TestService.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace FinRA.TestService.Services
{
    public class IssuerService : IIssuerService
    {
        private readonly IRepository _repository;

        public IssuerService(IRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<IssuerEntity>> GetIssuers()
        {
            return await _repository.Select<IssuerEntity>().ToListAsync();
        }

        public async Task<List<AssetEntity>> GetIssuersAssets(Guid id)
        {
            return await _repository.Select<AssetEntity>(x => x.Issuer.Id == id).Include(x => x.Issuer).Include(x=>x.StockExchange).ToListAsync();
        }
    }
}