﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FinRA.TestService.Controllers.Controllers
{
    [Authorize]
    [ApiController]
    public class ApiController : ControllerBase
    {
        protected ObjectResult Ok(object value) => StatusCode((int)HttpStatusCode.OK, value);
        protected ObjectResult Problem(string message) => StatusCode((int)HttpStatusCode.InternalServerError, message);
        
        protected async Task<ActionResult<T>> GetValidatedResponse<T>(T response, IValidator<T> validator) where T : class
        {
            var result = await validator.ValidateAsync(response);
            return result.IsValid
                ? Ok(response)
                : BadRequest(string.Join(", ", result.Errors.Select(failure => failure.ErrorMessage)));
        }
        
        protected async ValueTask ValidatedRequest<T>(T request, IValidator<T> validator) where T : class
        {
            var result = await validator.ValidateAsync(request);
            if (!result.IsValid)
            {
                throw new ValidationException("Request model validation failed",
                    result.Errors);
            }
        }
    }
}