﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using FinRA.TestService.Abstractions.Services;
using FinRA.TestService.Contracts.Models;
using FinRA.TestService.Contracts.Requests;
using FinRA.TestService.Contracts.Responses;
using FinRA.TestService.Contracts.Responses.Assets;
using FinRA.TestService.Controllers.Attributes;
using FinRA.TestService.Extensions;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FinRA.TestService.Controllers.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class AssetsController : ApiController
    {
        private readonly IValidator<SearchAssetsRequest> _searchAssetsRequestValidator;

        private readonly IAssetService _assetService;
        private readonly IMapper _mapper;
        public AssetsController(IValidator<SearchAssetsRequest> searchAssetsRequestValidator, IAssetService assetService, IMapper mapper)
        {
            _searchAssetsRequestValidator = searchAssetsRequestValidator;
            _assetService = assetService;
            _mapper = mapper;
        }

        /// <summary>
        /// Получение полной информации об активе по id
        /// </summary>
        /// <param name="id">Id актива</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<AssetFullInfoResponse>> GetAssetFullInfo(Guid id)
        {
            try
            {
                var result = await _assetService.GetAssetInfo(id);
                var response = _mapper.Map<AssetFullInfoResponse>(result);
                return Ok(response);
            }
            catch (Exception e)
            {
                return Problem(e.Message);
            }
        }
        
        /// <summary>
        /// Получение полной информации об активе по isin-коду
        /// </summary>
        /// <param name="isinCode">isin код</param>
        /// <returns></returns>
        [HttpGet("")]
        public async Task<ActionResult<AssetFullInfoResponse>> GetAssetFullInfoByIsin(string isinCode)
        {
            try
            {
                var result = await _assetService.GetAssetInfoByIsin(isinCode);
                if (result == null)
                    return Ok(null);
                
                var response = _mapper.Map<AssetFullInfoResponse>(result);
                return Ok(response);
            }
            catch (Exception e)
            {
                return Problem(e.Message);
            }
        }
        
        /// <summary>
        /// Поиск активов
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("search")]
        public async Task<ActionResult<SearchAssetsResponse>> SearchAssets([FromBody] SearchAssetsRequest request)
        {
            try
            {
                await ValidatedRequest(request,
                    _searchAssetsRequestValidator);

                var groupedAssets = await _assetService.SearchAssets(request.Search);
                if (groupedAssets == null || groupedAssets.Count == 0)
                    return Ok(null);
                
                var assetsGrouped = groupedAssets
                    .Select(groupedAsset 
                        => new GroupedAssetsResponse { AssetType = groupedAsset.Key.GetDescription(), Assets = _mapper.Map<List<AssetBaseInfoResponse>>(groupedAsset.Select(x => x).ToList()) })
                    .ToList();

                return Ok(new SearchAssetsResponse
                {
                  AssetsGrouped = assetsGrouped
                });
            }
            catch (Exception e)
            {
                return Problem(e.Message);
            }
        }
    }
}