﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FinRA.TestService.Abstractions.Services;
using FinRA.TestService.Contracts.Responses.Assets;
using FinRA.TestService.Contracts.Responses.Issuer;
using FinRA.TestService.Models.Entities;
using Microsoft.AspNetCore.Mvc;

namespace FinRA.TestService.Controllers.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class IssuerController : ApiController
    {
        private readonly IIssuerService _issuerService;
        private readonly IMapper _mapper;

        public IssuerController(IIssuerService issuerService, IMapper mapper)
        {
            _issuerService = issuerService;
            _mapper = mapper;
        }

        /// <summary>
        /// Получение перечня всеъ эмитентов
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public async Task<ActionResult<List<IssuerBaseInfoResponse>>> GetIssuers()
        {
            try
            {
                var res = await _issuerService.GetIssuers();
                var response = _mapper.Map<List<IssuerBaseInfoResponse>>(res);
                return Ok(response);
            }
            catch (Exception e)
            {
                return Problem(e.Message);
            }
        }
        
        /// <summary>
        /// Получение перечня всех активов эмитента
        /// </summary>
        /// <param name="id">Id эмитента</param>
        /// <returns></returns>
        [HttpGet("{id}/assets")]
        public async Task<ActionResult<List<AssetBaseInfoResponse>>> GetIssuersAssets(Guid id)
        {
            try
            {
                var assets = await _issuerService.GetIssuersAssets(id);
                var response = _mapper.Map<List<AssetBaseInfoResponse>>(assets);
                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
    }
}