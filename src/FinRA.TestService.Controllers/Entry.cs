﻿using System.Reflection;
using FinRA.TestService.Controllers.Controllers;
using FinRA.TestService.Controllers.Validators;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
namespace FinRA.TestService.Controllers
{
    public static class Entry
    {
        public static IMvcBuilder AddApi(this IMvcBuilder builder) => builder.AddApplicationPart(Assembly.GetAssembly(typeof(IssuerController)));

        public static IServiceCollection AddValidation(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddValidatorsFromAssemblyContaining<SearchAssetsValidator>(ServiceLifetime.Transient);

            return serviceCollection;
        }
    }
}