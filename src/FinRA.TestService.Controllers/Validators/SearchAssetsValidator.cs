﻿using FinRA.TestService.Contracts.Requests;
using FluentValidation;

namespace FinRA.TestService.Controllers.Validators
{
    public class SearchAssetsValidator : AbstractValidator<SearchAssetsRequest>
    {
        public SearchAssetsValidator()
        {
            RuleFor(request => request.Search)
                .NotNull().WithMessage("No Search parametrs");
            RuleFor(request => request.Search).NotEmpty().When(request => string.IsNullOrEmpty(request.Search))
                .WithMessage("Search parameter is empty");
            RuleFor(request => request.Search).MinimumLength(3).WithMessage("Search parameter cannot be less then 3 characters");
        }
    }
}