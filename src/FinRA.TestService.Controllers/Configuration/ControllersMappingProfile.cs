﻿using AutoMapper;
using FinRA.TestService.Contracts.Enums;
using FinRA.TestService.Contracts.Models;
using FinRA.TestService.Contracts.Responses.Assets;
using FinRA.TestService.Contracts.Responses.Issuer;
using FinRA.TestService.Enums;
using FinRA.TestService.Extensions;
using FinRA.TestService.Models;
using FinRA.TestService.Models.Entities;

namespace FinRA.TestService.Controllers.Configuration
{
    public class ControllersMappingProfile : Profile
    {
        public ControllersMappingProfile()
        {
            CreateMap<AssetEntity, AssetFullInfoResponse>(MemberList.Destination)
                .ForMember(dest => dest.AssetType, opt =>
                    opt.MapFrom(src => src.Type.GetDescription()))
                .ForMember(dest => dest.IssuerName, opt
                    => opt.MapFrom(src => src.Issuer.Name))
                .ForMember(dest => dest.StockExchangeCode, opt
                    => opt.MapFrom(src => src.StockExchange != null ? src.StockExchange.Code : null))
                .ForMember(dest => dest.Issuer, opt
                    => opt.MapFrom(src => src.Issuer != null
                        ? new Issuer
                        {
                            Country = src.Issuer.Country,
                            Id = src.Issuer.Id,
                            Industry = src.Issuer.Industry,
                            Name = src.Issuer.Name
                        } : null))
                .ForMember(dest => dest.Stock, opt
                    => opt.MapFrom(src => src.Type == AssetType.Stock
                        ? new Stock
                        {
                            Type = src.StockType.Value,
                            Description = src.StockType.Value.GetDescription()
                        } : null))
                .ForMember(dest => dest.Bond, opt
                    => opt.MapFrom(src => src.Type == AssetType.Bond
                        ? new Bond()
                        {
                            Type = src.BondType.Value,
                            Description = src.BondType.Value.GetDescription()
                        } : null))
                .ForMember(dest => dest.StockExchange, opt
                    => opt.MapFrom(src => src.StockExchange != null
                        ? new StockExchange
                        {
                            Name = src.StockExchange.Name,
                            Code = src.StockExchange.Code
                        } : null));

            CreateMap<AssetEntity, AssetBaseInfoResponse>(MemberList.Destination)
                .ForMember(dest => dest.AssetType, opt =>
                    opt.MapFrom(src => src.Type.GetDescription()))
                .ForMember(dest => dest.IssuerName, opt
                    => opt.MapFrom(src => src.Issuer.Name))
                .ForMember(dest => dest.StockExchangeCode, opt
                    => opt.MapFrom(src => src.StockExchange != null ? src.StockExchange.Code : null));

            CreateMap<IssuerEntity, IssuerBaseInfoResponse>(MemberList.Destination);
        }
    }
}